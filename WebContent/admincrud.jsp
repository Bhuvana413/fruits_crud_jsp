<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin CRUD</title>
<style type="text/css">
body{
background-color:#aaf2bd;}
</style>
</head>
<body>
	<h1>Admin CRUD Operations Page</h1>
	<p align="right">
		Hi,
		<%=(Integer) session.getAttribute("uid")%>
		<a href="home">Home</a> <a href="insert">Add New Fruits</a> <a
			href="signout">Logout</a>
	</p>
	<hr />
	${msg}
	<table border="5">
		<tr>
			<th colspan="8">Fruits Details</th>
		</tr>
		<tr>
			<th>Fruits ID</th>
			<th>Fruits Name</th>
			<th>Fruits Color</th>
			<th>Fruits Price</th>
			<th>Fruits Place</th>
			<th>Fruits Quantity</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${Fruits}" var="f">
			<tr style="text-align: center;">
				<td>${f.getFid()}</td>
				<td>${f.getFname()}</td>
				<td>${f.getColor()}</td>
				<td>${f.getFprice()}</td>
				<td>${f.getFplace()}</td>
				<td>${f.getFquantity()}</td>
				<td><a style="text-decoration: none"
					href="update?fid=${f.getFid()}&fprice=${f.getFprice()}&fplace=${f.getFplace()}&fquantity=${f.getFquantity()}">Update</a></td>
				<td><a style="text-decoration: none"
					href="delete?fid=${f.getFid()}">Delete</a>
				</td>
			</tr>

		</c:forEach>
	</table>
</body>
</html>



