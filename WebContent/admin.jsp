<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
<style type="text/css">
body{
background-color:#aaf2bd;
}
</style>
</head>
<body>
	<h1>Admin Home Page</h1>
	<p align="right">
		Hi,
		<%=(Integer) session.getAttribute("uid")%>
		<a href="admincrud">Admin CRUD</a> <a
			href="signout">Logout</a>
	</p>
	<hr />

	<table border="5">
		<tr>
			<th colspan="6">Fruits Details</th>
		</tr>
		<tr>
			<th>Fruits ID</th>
			<th>Fruits NAME</th>
			<th>Fruits COLOR</th>
			<th>Fruits PRICE</th>
			<th>Fruits PLACE</th>
			<th>Fruits QUANTITY</th>
		</tr>
		<c:forEach items="${Fruits}" var="f">
			<tr style="text-align: center;">
				<td>${f.getFid()}</td>
				<td>${f.getFname()}</td>
				<td>${f.getColor()}</td>
				<td>${f.getFprice()}</td>
				<td>${f.getFplace()}</td>
				<td>${f.getFquantity()}</td>

		 	</tr>

		</c:forEach>
	</table>
</body>
</html>