<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update</title>
<style type="text/css">
body {background-color : #aaf2bd;
	
}
</style>
</head>
<body>
	<h1>Admin Update Page</h1>
	Hi,
	<%=(Integer) session.getAttribute("uid")%><hr />
	<form action="updateCont" method="post">
		<table>
			<tr>
				<td>Fruits ID:</td>
				<td><input type="text" name="fid" readonly="readonly"
					value="${fruits.getFid()}" /></td>
			</tr>
			<tr>
				<td>Fruits PRICE :</td>
				<td><input type="text" name="fprice"
					value="${fruits.getFprice()}"></td>
			</tr>
			<tr>
				<td>Fruits PLACE :</td>
				<td><input type="text" name="fplace"
					value="${fruits.getFplace()}"></td>
			</tr>
			<tr>
				<td>Fruits QUANTITY :</td>
				<td><input type="text" name="fquantity"
					value="${fruits.getFquantity()}"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Update" /></td>
			</tr>
		</table>


	</form>
</body>
</html>