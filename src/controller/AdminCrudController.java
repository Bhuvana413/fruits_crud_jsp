package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IFruitsDao;
import dao.IFruitsDaoImpl;
import model.Fruits;

@WebServlet({ "/admincrud", "/home", "/delete", "/insert", "/signout",
    "/update" })
public class AdminCrudController extends HttpServlet {
  private static final long serialVersionUID = 1L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String url = request.getServletPath();
    IFruitsDao dao = new IFruitsDaoImpl();
    Fruits fruits = new Fruits();

    if (url.equals("/admincrud")) {

      List<Fruits> list = dao.viewAll();

      request.setAttribute("Fruits", list);
      request.getRequestDispatcher("admincrud.jsp").forward(request, response);
    } else if (url.equals("/home")) {
      List<Fruits> list = dao.viewAll();
      request.setAttribute("Fruits", list);
      request.getRequestDispatcher("admin.jsp").forward(request, response);
    } else if (url.equals("/delete")) {
      int fid = Integer.parseInt(request.getParameter("fid"));
      fruits.setFid(fid);
      dao.delete(fruits);
      PrintWriter out = response.getWriter();
      response.setContentType("text/html");
      out.print(fid + " deleted successfully<br/>");
      List<Fruits> list = dao.viewAll();
      request.setAttribute("Fruits", list);
      request.getRequestDispatcher("admincrud.jsp").include(request, response);
      // request.getRequestDispatcher("admincrud").include(request, response);
    } else if (url.equals("/insert")) {
      request.getRequestDispatcher("insert.jsp").forward(request, response);
    } else if (url.equals("/signout")) {
      HttpSession session = request.getSession(false);
      Integer uid = (Integer) session.getAttribute("uid");
      System.out.println(uid + " logged out @ " + new Date());
      session.invalidate();
      request.setAttribute("uid", uid + "  Logged out successfully!!!");
      request.getRequestDispatcher("logout.jsp").forward(request, response);
    } else if (url.equals("/update")) {
      int fid = Integer.parseInt(request.getParameter("fid"));
      double fprice = Double.parseDouble(request.getParameter("fprice"));
      String fplace = request.getParameter("fplace");
      String fquantity = request.getParameter("fquantity");
      Fruits fruits1 = new Fruits(fid, fprice, fplace,
          fquantity);
      request.setAttribute("fruits", fruits1);
      request.getRequestDispatcher("update.jsp").forward(request, response);
    }

  }

}
