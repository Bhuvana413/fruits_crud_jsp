package controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IFruitsDao;
import dao.IFruitsDaoImpl;
import model.Admin;
import model.Fruits;

@WebServlet(name = "AdminController", urlPatterns = { "/login", "/add",
    "/updateCont" })
public class AdminController extends HttpServlet {
  private static final long serialVersionUID = 1L;

  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
    String url = request.getServletPath();
    IFruitsDao dao = new IFruitsDaoImpl();

    if (url.equals("/login")) {
      Integer uid = Integer.parseInt(request.getParameter("uid"));
      String password = request.getParameter("password");
     
      HttpSession session = request.getSession(true);
      session.setAttribute("uid", uid);
      System.out
          .println(uid + "Logged in @" + new Date(session.getCreationTime()));

      Admin admin = new Admin(uid, password);
      int result = dao.adminAuthentication(admin);
      if (result > 0) {
        List<Fruits> list = dao.viewAll();
        request.setAttribute("Fruits", list);
        request.getRequestDispatcher("admin.jsp").forward(request, response);
      } else {
        request.setAttribute("error",
            "Hi" + uid + ", Please check your credentials");
        request.getRequestDispatcher("index.jsp").forward(request, response);
      }
    } else if (url.equals("/add")) {
      int fid = Integer.parseInt(request.getParameter("fid"));
      String fname = request.getParameter("fname");
      String color = request.getParameter("color");
      double fprice = Double.parseDouble(request.getParameter("fprice"));
      
      String fplace = request.getParameter("fplace");
      String fquantity = request.getParameter("fquantity");
      Fruits fruits = new Fruits(fid, fname, color, fprice, fplace, fquantity);
      int result = dao.add(fruits);
      if (result > 0) {
        request.setAttribute("msg", fid + "inserted succesfully");
      } else {
        request.setAttribute("msg", fid + "duplicate entry");
      }
      List<Fruits> list = dao.viewAll();
      request.setAttribute("Fruits", list);
      request.getRequestDispatcher("admincrud.jsp").forward(request, response);
    } else if (url.equals("/updateCont")) {
      int fid = Integer.parseInt(request.getParameter("fid"));
      double fprice = Double.parseDouble(request.getParameter("fprice"));
      String fplace = request.getParameter("fplace");
      String fquantity = request.getParameter("fquantity");
     // System.out.println(fid+fprice+fplace+fquantity);
     Fruits fruits=new Fruits(fid, fprice, fplace, fquantity);
     // System.out.println(f.getFplace()+f.getFid()+f.getFprice()+f.getFquantity());
      dao.update(fruits);
      List<Fruits> list = dao.viewAll();
      request.setAttribute("Fruits", list);
      request.getRequestDispatcher("admincrud.jsp").include(request, response);

    }
  }

}
