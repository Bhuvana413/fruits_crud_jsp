package model;

public class Fruits {
  private int fid;
  private String fname;
  private String color;
  private double fprice;
  private String fplace;
  private String fquantity;
  public Fruits() {
    super();
    // TODO Auto-generated constructor stub
  }
  
  public Fruits(int fid, double fprice, String fplace, String fquantity) {
 
    this.fid = fid;
    this.fprice = fprice;
    this.fplace = fplace;
    this.fquantity = fquantity;
  }

  public Fruits(int fid, String fname, String color, double fprice,
      String fplace, String fquantity) {
    super();
    this.fid = fid;
    this.fname = fname;
    this.color = color;
    this.fprice = fprice;
    this.fplace = fplace;
    this.fquantity = fquantity;
  }
  
  public int getFid() {
    return fid;
  }
  public void setFid(int fid) {
    this.fid = fid;
  }
  public String getFname() {
    return fname;
  }
  public void setFname(String fname) {
    this.fname = fname;
  }
  public String getColor() {
    return color;
  }
  public void setColor(String color) {
    this.color = color;
  }
  public double getFprice() {
    return fprice;
  }
  public void setFprice(double fprice) {
    this.fprice = fprice;
  }
  public String getFplace() {
    return fplace;
  }
  public void setFplace(String fplace) {
    this.fplace = fplace;
  }
  public String getFquantity() {
    return fquantity;
  }
  public void setFquantity(String fquantity) {
    this.fquantity = fquantity;
  }
  


}
