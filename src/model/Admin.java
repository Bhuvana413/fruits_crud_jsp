package model;

public class Admin {
  private Integer uid;
  private String password;
  public Admin() {
    // TODO Auto-generated constructor stub
  }
  
  public Admin(Integer uid, String password) {
    super();
    this.uid = uid;
    this.password = password;
  }

  public Integer getUid() {
    return uid;
  }
  public void setUid(Integer uid) {
    this.uid = uid;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  
  
  

}
