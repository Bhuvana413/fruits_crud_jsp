package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.Fruits;
import util.DbUtil;
import util.QueryUtil;

public class IFruitsDaoImpl implements IFruitsDao {
  int result;
  PreparedStatement pst;
  ResultSet rs;
 
  @Override
  public int adminAuthentication(Admin admin) {
    result = 0;
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.adminLogin);
      pst.setInt(1, admin.getUid());
      pst.setString(2, admin.getPassword());
      rs = pst.executeQuery();
      while (rs.next()) {
        result++;
        
      }

    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception oocurs in Admin Authentication");
      //e.printStackTrace();
    } 
    return result;
  }

  @Override
  public List<Fruits> viewAll() {
    List<Fruits> list = new ArrayList<Fruits>();
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.viewAll);
      rs = pst.executeQuery();
      while (rs.next()) {
        Fruits fruits = new Fruits(rs.getInt(1), rs.getString(2), rs.getString(3),
            rs.getDouble(4), rs.getString(5), rs.getString(6));
       
        list.add(fruits);
        
      }
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occurs in view All Fruits");
    }
    return list;
  }

  @Override
  public int add(Fruits info) {
   try {
    pst = DbUtil.getCon().prepareStatement(QueryUtil.add);
    pst.setInt(1, info.getFid());
    pst.setString(2, info.getFname());
    pst.setString(3, info.getColor());
    pst.setDouble(4, info.getFprice());
    pst.setString(5, info.getFplace());
    pst.setString(6, info.getFquantity());
    result = pst.executeUpdate();
  
  } catch (ClassNotFoundException | SQLException e) {
   
  }
   
    return result;
  }



  @Override
  public void delete(Fruits info) {
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.delete);
      pst.setInt(1, info.getFid());
      pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      
    }
  }

  @Override
  public void update(Fruits edit) {
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.update);
      pst.setString(1,edit.getFplace());
      pst.setDouble(2,edit.getFprice());
      pst.setString(3,edit.getFquantity());
      pst.setInt(4,edit.getFid());
      pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      
    }
    
  }

}
