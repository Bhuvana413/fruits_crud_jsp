package dao;

import java.util.List;

import model.Admin;
import model.Fruits;



public interface IFruitsDao {
  public int adminAuthentication(Admin admin);

  public List<Fruits> viewAll();

  public int add(Fruits info);

  public void update(Fruits edit);

  public void delete(Fruits info);


}
